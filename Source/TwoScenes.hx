package;

import mme.math.glmatrix.Vec3;
import lime.utils.Assets;
import lime.app.Application;
import lime.utils.Log;

import lime.graphics.RenderContext;
import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

import lime.ui.MouseButton;
import lime.ui.KeyCode;
import lime.ui.KeyModifier;


import mme.core.Renderer;
import mme.core.Mesh;
import mme.core.Scene;
import mme.core.Camera;

import mme.material.TextureMaterial;
import mme.material.BasicMaterial;
import mme.material.PhongMaterial;

import mme.geometry.Triangle;
import mme.geometry.Rectangle;
import mme.geometry.Square;
import mme.geometry.MeshGeometry;

import mme.camera.PerspectiveCamera;
import mme.camera.OrthoCamera;

import mme.light.AmbientLight;
import mme.light.PointLight;

import mme.util.loader.WaveObjLoader;
import mme.util.loader.StlLoader;


class TwoScenes extends Application {

    private var renderer : Renderer;

    private var perspectiveScene : Scene;
    private var perspectiveCamera : Camera;

    private var objMeshGeometry : MeshGeometry;
    private var stlMeshGeometry : MeshGeometry;

    private var orthoScene : Scene;
    private var orthoCamera : Camera;

    private var square : Square;
    private var rectangle : Rectangle;
    private var triangle : Triangle;

    public function new() {

        super();

        trace("Mini-Mighty-Engine v0.1 - Two scenes demo");
        Log.info("Mini-Mighty-Engine v0.1 - Two scenes demo");
    }

    public override function onRenderContextLost() : Void {
        trace("onRenderContextLost");
    }

    public override function onRenderContextRestored( context:RenderContext ) : Void {
        trace("onRenderContextRestored");
    }

    public override function onPreloadComplete() : Void {
        trace("onPreloadComplete");

        init();
    }

    private function init() : Void {

        initApp();


        switch( window.context.type ) {

            case CAIRO:
                Log.error("Render context not supported: CAIRO");
                window.close();

            case CANVAS:
                Log.error("Render context not supported: CANVAS");
                window.close();

            case DOM:
                Log.error("Render context not supported: DOM");
                window.close();

            case FLASH:
                Log.error("Render context not supported: FLASH");
                window.close();

            case OPENGL, OPENGLES, WEBGL:

            #if ( lime_opengl || lime_opengles )
                trace( "OpenGL version: " + GL.getString( GL.VERSION ) );
                trace( "GLSL version: " + GL.getString( GL.SHADING_LANGUAGE_VERSION ) );
            #end

                var gl = window.context.webgl;

                renderer = new Renderer( gl );

                initScene( gl );

            default:

                Log.error("Current render context not supported by this sample");
                window.close();

        }
    }

    private function initApp() : Void {
    }

    private function initScene( gl : WebGLRenderContext ) : Void {


        var orange = new BasicMaterial( 0xFF7F00FF );
        var green = new BasicMaterial( 0x00FF00FF );

        var distanceA = 550.;
        var distanceB = 200.;

        var phongGreen = new PhongMaterial( 0x00ff00ff );
        var phongOrange = new PhongMaterial( 0xfc9b0aff );



		//
		// PERSPECTIVE SCENE
		//

        perspectiveScene = new Scene();

        var aspect = window.width / window.height;

        perspectiveCamera = new PerspectiveCamera( 75., aspect, 1., 2000. );
        perspectiveCamera.translate( [ 0., 0., 880. ] );
        perspectiveScene.addCamera( perspectiveCamera );


        var ambientLight = new AmbientLight( 0xffffffff, 0.1 );
        perspectiveScene.addLight( ambientLight );

        var pointLight = new PointLight( 0xffffffff, 1.0 );
        pointLight.translate( [ 100.0, 0.0, 700.0 ] );
        perspectiveScene.addLight( pointLight );

        var objLoader = new WaveObjLoader();
        var objMesh : Mesh;


        // objMesh = objLoader.load( "assets/models/cuboid.obj", Assets.getBytes );
        // objMesh.scaleTo( 50., 50., 50. );


        objMesh = objLoader.loadAsMesh( "assets/models/icosahedron.obj", Assets.getBytes );
        objMesh.scaleTo( 50., 50., 50. );

        objMeshGeometry = new MeshGeometry( objMesh, phongOrange );
        objMeshGeometry.translate( [ -75., 50., 560. ] );
        objMeshGeometry.rotate( 45, Vec3.Y_AXIS );
        objMeshGeometry.rotate( 30, Vec3.X_AXIS );
        perspectiveScene.add( objMeshGeometry );


        var stlLoader = new StlLoader();
        var stlMesh = stlLoader.load( 'assets/models/hole.stl' );
        stlMesh.scaleTo( 2., 2., 2. );

        stlMeshGeometry = new MeshGeometry( stlMesh, phongGreen );
        stlMeshGeometry.translate( [ 0., -100., 570. ] );
        perspectiveScene.add( stlMeshGeometry );





		//
		// ORTHO SCENE
		//
        orthoScene = new Scene();

        orthoCamera = new OrthoCamera( 0., 1000. * aspect, 0., 1000., 2000. );
        orthoCamera.translate([ 0, 0, 1000 ]);

        orthoScene.addCamera( orthoCamera );

        square = new Square( 200., new TextureMaterial( Assets.getImage("assets/UV_Grid_1024.jpeg") ) );
        // square.translate([ 0., 0.0, -100.0 ]);
        square.translate([ 850., 850.0, 50.0 ]);
        orthoScene.add( square );

        rectangle = new Rectangle( 900., 50., orange );
        // rectangle.translate([ 0., 0., 100.0 ]);
        rectangle.translate([ 500., 925., 75.0 ]);
        orthoScene.add( rectangle );


        triangle = new Triangle(
            // vertex 0
            [ -100., -100., 0.0 ],
            // vertex 1
            [ -100., 100., 0.0 ],
            // vertex 2
            [ 100., -100., 0.0 ],
            // lime color
            new BasicMaterial( 0x6fac17ff )
        );
        // triangle.translate([ 0.0, 0.0, 200.0 ]);
        triangle.translate([ 150.0, 850.0, 100.0 ]);
        orthoScene.add( triangle );
    }

    public override function update( deltaTime : Int ) : Void {

        if( !preloader.complete ) return;

        // deltaTime is in milliseconds
        var rotationSpeed = 90.0; // in degrees per second
        var delta = deltaTime / 1000.0;

        objMeshGeometry.rotate( delta * rotationSpeed, Vec3.Y_AXIS );
        stlMeshGeometry.rotate( delta * rotationSpeed, Vec3.X_AXIS );
    }

    public override function onMouseDown( x : Float, y : Float, button : MouseButton ) : Void {

        var rotation = 10.0; // in degrees

        objMeshGeometry.rotate( rotation, Vec3.Y_AXIS );
        stlMeshGeometry.rotate( rotation, Vec3.X_AXIS );
    }

    public override function onKeyDown( keyCode : KeyCode, modifier : KeyModifier ) : Void {

        var rotation = 5.0; // in degrees

        switch( keyCode ) {

            case KeyCode.ESCAPE | KeyCode.Q:
                window.close();

            case KeyCode.UP:
                perspectiveCamera.rotate( rotation, Vec3.X_AXIS );

            case KeyCode.DOWN:
                perspectiveCamera.rotate( - rotation, Vec3.X_AXIS );

            case KeyCode.RIGHT:
                perspectiveCamera.rotate( - rotation, Vec3.Y_AXIS );

            case KeyCode.LEFT:
                perspectiveCamera.rotate( rotation, Vec3.Y_AXIS );
            
            default:
        }
        
    }

    public override function render( context : RenderContext ) : Void {

        if( !preloader.complete ) return;

        switch( context.type ) {

            case OPENGL, OPENGLES, WEBGL:

                var gl = context.webgl;

                renderer.renderSetup( gl );
                renderer.renderScene( gl, perspectiveScene );
                renderer.renderScene( gl, orthoScene );
            default:

        }
    }
}
