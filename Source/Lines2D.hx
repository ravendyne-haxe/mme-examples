package;


import mme.math.glmatrix.Vec3;
import mme.math.glmatrix.Vec2;
import mme.core.Renderer;
import mme.g2d.geometry.PolyLine2D;
import mme.camera.OrthoCamera;
import mme.core.Scene;
import lime.ui.KeyCode;
import lime.ui.KeyModifier;
import lime.ui.MouseButton;

import lime.app.Application;
import lime.graphics.RenderContext;
import lime.graphics.WebGLRenderContext;

using mme.math.glmatrix.Vec2Tools;

class QuadraticBezier {

	var controls : Array<Vec2>;

	public function new( P0 : Vec2, P1 : Vec2, P2 : Vec2 ) {
		controls = [];
		controls.push( P0 );
		controls.push( P1 );
		controls.push( P2 );
	}

    public function B( t : Float ) : Vec2 {

        if(t <= 0.0) return controls[ 0 ].clone();
        if(t >= 1.0) return controls[ 2 ].clone();

        var A = (1.0 - t) * (1.0 - t);
        var B = 2.0 * (1.0 - t) * t;
        var C = t * t;
        
        // var result = 
        //         controls[ 0 ].multiply(A).add(
        //         controls[ 1 ].multiply(B).add(
        //         controls[ 2 ].multiply(C)));

		// P0 * A + P1 * B + P2 * C
        // var p0 = new Vec2();
		// p0.scale( controls[ 0 ], A );
		var p0 = controls[ 0 ].scale( A );
        // var p1 = new Vec2();
		// p1.scale( controls[ 1 ], B );
		var p1 = controls[ 1 ].scale( B );
        // var p2 = new Vec2();
		// p2.scale( controls[ 2 ], C );
		var p2 = controls[ 2 ].scale( C );
        
		// var result = new Vec2();
		// result.add( p0, p1 );
		// result.add( result, p2 );
		var result = p0.add( p1 ).add( p2 );

        return result;
    }
}

class CubicBezier {

	var controls : Array<Vec2>;

	public function new( P0 : Vec2, P1 : Vec2, P2 : Vec2, P3 : Vec2 ) {
		controls = [];
		controls.push( P0 );
		controls.push( P1 );
		controls.push( P2 );
		controls.push( P3 );
	}

    public function B( t : Float ) : Vec2 {

        if(t <= 0.0) return controls[ 0 ].clone();
        if(t >= 1.0) return controls[ 3 ].clone();

        var A = (1.0 - t) * (1.0 - t) * (1.0 - t);
        var B = 3.0 * (1.0 - t) * (1.0 - t) * t;
        var C = 3.0 * (1.0 - t) * t * t;
        var D = t * t * t;
        
        // var result = 
        //         controls[ 0 ].multiply(A).add(
        //         controls[ 1 ].multiply(B).add(
        //         controls[ 2 ].multiply(C).add(
        //         controls[ 3 ].multiply(D))));

		// P0 * A + P1 * B + P2 * C + P3 * D
        // var p0 = new Vec2();
		// p0.scale( controls[ 0 ], A );
		var p0 = controls[ 0 ].scale( A );
        // var p1 = new Vec2();
		// p1.scale( controls[ 1 ], B );
		var p1 = controls[ 1 ].scale( B );
        // var p2 = new Vec2();
		// p2.scale( controls[ 2 ], C );
		var p2 = controls[ 2 ].scale( C );
        // var p3 = new Vec2();
		// p3.scale( controls[ 3 ], D );
		var p3 = controls[ 3 ].scale( D );
        
		// var result = new Vec2();
		// result.add( p0, p1 );
		// result.add( result, p2 );
		// result.add( result, p3 );
		var result = p0.add( p1 ).add( p2 ).add( p3 );

        return result;
    }
}


class Lines2D extends Application {

	var scene : Scene;
	var camera : OrthoCamera;
	var renderer : Renderer;
	var polyline : PolyLine2D;

    private var rotationSpeed : Float;

	public function new () {
		
		super ();		
	}

    public override function onPreloadComplete() : Void {

        rotationSpeed = 45.0; // in degrees per second

		poly();
    }



	public function poly() {

		var gl = window.context.webgl;

		renderer = new Renderer(gl);

		scene = new Scene();

        var aspect = window.width / window.height;

		camera = new OrthoCamera( -1000. * aspect, 1000. * aspect, -1000., 1000., 2000. );
        camera.translate([0,0,1000]);

		scene.addCamera( camera );

		polyline = new PolyLine2D( 0xead446ff, 20.0 );

        // polyline.addPoint([ -500., 0. ]).addPoint([ 500., 0. ]).addPoint([ 750., 0. ]);
		// polyline.addPoint([ -500., -500. ]).addPoint([ 500., -500. ]).addPoint([ 0., 500. ]).close();
		polyline.addPoints([ [ -500., -500. ], [ 500., -500. ], [ 0., 500. ] ]).close();

		scene.add(polyline);

		// set black outline
		var bezierPoly = new PolyLine2D( 0x9d46eaff, 50.0, 0x000000ff );
		var qbc = new QuadraticBezier( [700.0, 0.0], [0.0, 0.0], [0.0, 700.0] );
		for( idx in 0...10) {
			var t = idx * 0.1;
			var point = qbc.B( t );
			bezierPoly.addPoint( point );
		}
		scene.add(bezierPoly);


		// set black outline
		bezierPoly = new PolyLine2D( 0x4bea46ff, 20.0, 0x000000ff );
		var cbc = new CubicBezier( [700.0, 0.0], [0.0, 0.0], [0.0, 700.0], [ -700.0, 700.0 ] );
		for( idx in 0...10) {
			var t = idx * 0.1;
			var point = cbc.B( t );
			bezierPoly.addPoint( point );
		}
		scene.add(bezierPoly);
		bezierPoly.translate([ 0.0, -100.0, 0.0 ]);
		
	}

    public override function update( deltaTime : Int ) : Void {

        if( !preloader.complete ) return;

		// deltaTime is in milliseconds
		var delta = deltaTime / 1000.0;
		var angle = delta * rotationSpeed;

		polyline.rotate( angle, Vec3.Z_AXIS );
	}

	public override function render (context:RenderContext):Void {

		if (!preloader.complete) return;
		
		switch (context.type) {
			
			case OPENGL, OPENGLES, WEBGL:

				var gl = context.webgl;
				renderer.render( gl, scene );

			default:
		}	
	}

    public override function onMouseDown( x : Float, y : Float, button : MouseButton ) : Void {
    }

    public override function onKeyDown( keyCode : KeyCode, modifier : KeyModifier ) : Void {

        switch( keyCode ) {

            case KeyCode.ESCAPE | KeyCode.Q:
                window.close();
            
            default:
        }
        
    }	
}