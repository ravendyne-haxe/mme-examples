package;

import mme.extras.core.OrthoScene;
import haxe.Timer;
import mme.MMEngine;
import mme.extras.camera.CameraTools;
import mme.core.SceneObject;
import mme.light.AmbientLight;
import lime.utils.Log;
import lime.graphics.opengl.GL;
import mme.material.NamedColor;
import mme.core.Group;
import mme.material.BasicMaterial;
import mme.geometry.Rectangle;
import mme.core.Renderer;
import mme.g2d.geometry.PolyLine2D;
import mme.geometry.PolyLine;
import mme.geometry.Plane;
import mme.camera.OrthoCamera;
import mme.core.Scene;
import lime.ui.KeyCode;
import lime.ui.KeyModifier;
import lime.ui.MouseButton;

import lime.app.Application;
import lime.graphics.RenderContext;

using mme.math.glmatrix.Vec3Tools;

import mme.camera.PerspectiveCamera;
import mme.core.Camera;
import mme.light.PointLight;

import mme.geometry.Box;

import mme.extras.camera.OrbitCameraController;

import mme.geometry.Plane;
import mme.core.Group;
import lime.utils.Log;
import mme.core.BoundingBox;
import lime.utils.Assets;
import mme.math.glmatrix.Vec3;
import mme.util.Utils;
import mme.util.loader.WaveObjLoader;
import mme.core.Scene;



class Terrain extends Application {

    var scene : Scene;
    var ortho_camera : Camera;
    var persp_camera : Camera;
    var light : PointLight;

    var renderer : Renderer;

    var o_ctrlr : OrbitCameraController;
    var p_ctrlr : OrbitCameraController;

    public function new() {

        super();

        trace("Mini-Mighty-Engine - Main.hx");

    }

    override function onWindowCreate() {

        var gl = window.context.webgl;
        renderer = new Renderer( gl );
    }

    // override function onPreloadProgress( loaded : Int, total : Int ) {
    //     Log.debug('$loaded of $total');
    // }

    public override function onPreloadComplete() : Void {
        trace("onPreloadComplete");
    #if ( lime_opengl || lime_opengles )
        trace( "OpenGL version: " + GL.getString( GL.VERSION ) );
        trace( "GLSL version: " + GL.getString( GL.SHADING_LANGUAGE_VERSION ) );
    #end
        trace( window.context.type );
        trace( window.context.version );
        
        // Text.createFontAtlasBinary();

        var start = Timer.stamp();

        MMEngine.initialize();

        createScene();
        makeSceneContent();

        var delta = Timer.stamp() - start;
        Log.info('load -> ${delta * 1000} ms');
    }

    function createScene() {

        scene = new Scene();

        var aspect = window.width / window.height;

        ortho_camera = new OrthoCamera( 0, 1000 * aspect, 0, 1000, 10000.0 );
        // ortho_camera = new OrthoCamera( -500, 500, -500, 500, 2000.0 );
        ortho_camera.translateZ( 1000 );
        scene.addCamera( ortho_camera );
        // Log.info('ortho_camera ${ortho_camera.scenePosition.str()}');
        Log.info('ortho_camera ${ortho_camera.scenePosition}');

        o_ctrlr = new OrbitCameraController( ortho_camera, window );
        scene.add( o_ctrlr );

        // persp_camera = new PerspectiveCamera( 60, 1, 1, 10000 );
        persp_camera = new PerspectiveCamera( 60, aspect, 1, 10000 );
        // persp_camera.translateZ( 1000 );
        persp_camera.translate([ 500, 500, 1000 ]);
        scene.addCamera( persp_camera );
        // Log.info('persp_camera ${persp_camera.scenePosition.str()}');
        Log.info('persp_camera ${persp_camera.scenePosition}');

        p_ctrlr = new OrbitCameraController( persp_camera, window );
        scene.add( p_ctrlr );



        //
        // POINT LIGHTS
        //
        light = new PointLight();
        // light.translate([ 10000, 0, 10000 ]);
        // light.translate([ -100, 0, 300 ]);
        // light.translate([ 750, 750, 500 ]);
        light.translate([ 500, 750, -400 ]);
        scene.addLight( light );
        // var light_marker = new Square( 20, new BasicMaterial( NamedColor.yellow ) );
        var light_marker = new Box( 10, 10, new BasicMaterial( NamedColor.yellow ) );
        light.add( light_marker );

        scene.add( light );
        // OR
        // p_ctrlr.add( light );


        light = new PointLight();
        light.translate([ -500, 750, -400 ]);
        scene.addLight( light );
        var light_marker_2 = new Box( 10, 10, new BasicMaterial( NamedColor.yellow ) );
        light.add( light_marker_2 );

        scene.add( light );
        // OR
        // p_ctrlr.add( light );


        //
        // AMBIENT LIGHT
        //
        var ambient = new AmbientLight();
        scene.addLight( ambient );

        // light.intensity = 0;
        // ambient.intensity = 0;

        // light.intensity = 9/16;
        // ambient.intensity = 0.15;
    }

    function loadTerrain( scene : Scene ) {
        var loader = new WaveObjLoader();

        var terrain = loader.load( "terrain.obj", function(name) return Assets.getBytes('assets/terrain/$name') );
        Utils.scaleAllTo( terrain, [ 100, 100, 100 ] );
        terrain.translate([ 400, 200, 0 ]);
        terrain.rotate( 45, Vec3.Y_AXIS );

        scene.add( terrain );
    }

    function makeSceneContent() {
        var start = Timer.stamp();

        //
        // OBJECTS
        //
        loadTerrain( scene );

        var delta = Timer.stamp() - start;
        Log.info('makeSceneContent -> ${delta * 1000} ms');
    }

    public override function onWindowResize( width : Int, height : Int ) {
        // renderer.viewport( window.context.webgl, width, height );
    }

    public override function onMouseDown(x:Float, y:Float, button:MouseButton) {
        Log.info(' mouse down: $x, $y ');
        var ray = CameraTools.ray( ortho_camera, Std.int(x), Std.int(y), window.width, window.height );
        Log.info('ortho ray: ${ray.origin}, ${ray.direction} ');
        ray = CameraTools.ray( persp_camera, Std.int(x), Std.int(y), window.width, window.height );
        Log.info('persp ray: ${ray.origin}, ${ray.direction} ');
    }

    public override function onKeyDown( keyCode : KeyCode, modifier : KeyModifier ) : Void {

        var rotation = 5.0; // in degrees

        switch( keyCode ) {

            case ESCAPE, Q:
                window.close();


            case W:
                // plate.rotate( -10, Vec3.X_AXIS );
                var whereTo = scene.activeCamera.cameraLook.scale(20);
                p_ctrlr.translate(whereTo);
                o_ctrlr.translate(whereTo);

            case S:
                // plate.rotate( 10, Vec3.X_AXIS );
                var whereTo = scene.activeCamera.cameraLook.scale(-20);
                p_ctrlr.translate(whereTo);
                o_ctrlr.translate(whereTo);

            case A:
                // plate.rotate( -10, Vec3.Y_AXIS );
                var whereTo = scene.activeCamera.cameraLook.cross( scene.activeCamera.cameraUp ).scale(-20);
                p_ctrlr.translate(whereTo);
                o_ctrlr.translate(whereTo);

            case D:
                // plate.rotate( 10, Vec3.Y_AXIS );
                var whereTo = scene.activeCamera.cameraLook.cross( scene.activeCamera.cameraUp ).scale(20);
                p_ctrlr.translate(whereTo);
                o_ctrlr.translate(whereTo);

            case C:
                if( scene.activeCamera == persp_camera ) {
                    scene.activeCamera = ortho_camera;
                } else {
                    scene.activeCamera = persp_camera;
                }

            case R:
                p_ctrlr.reset();
                p_ctrlr.position = [0,0,0];
                o_ctrlr.reset();
                o_ctrlr.position = [0,0,0];

            case UP:
                p_ctrlr.orbitTilt( rotation );
                o_ctrlr.orbitTilt( rotation );

            case DOWN:
                p_ctrlr.orbitTilt( - rotation );
                o_ctrlr.orbitTilt( - rotation );

            case RIGHT:
                if( modifier.ctrlKey ) {
                    p_ctrlr.orbitPan( - 45 );
                    o_ctrlr.orbitPan( - 45 );
                }
                else{
                    p_ctrlr.orbitPan( - rotation );
                    o_ctrlr.orbitPan( - rotation );
                }

            case LEFT:
                if( modifier.ctrlKey ){
                    p_ctrlr.orbitPan( 45 );
                    o_ctrlr.orbitPan( 45 );
                }
                else{
                    p_ctrlr.orbitPan( rotation );
                    o_ctrlr.orbitPan( rotation );
                }

            default:
        }
    }

    public override function render( context : RenderContext ) : Void {

	    if (!preloader.complete) {
            return;
        }

	    switch (context.type) {

		    case OPENGL, OPENGLES, WEBGL:

                var gl = context.webgl;

                if( renderer != null )
                renderer.render( gl, scene );
                else {
                    gl.clearColor( 0, 1, 0, 1 );
                    gl.clear( GL.COLOR_BUFFER_BIT );
                }


		    default:

		}
	}
}
