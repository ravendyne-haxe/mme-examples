package;

import mme.core.SceneNode;
import mme.core.Group;
import mme.math.glmatrix.Vec3;
import lime.utils.Assets;
import lime.app.Application;
import lime.utils.Log;

import lime.graphics.RenderContext;
import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

import lime.ui.MouseButton;
import lime.ui.KeyCode;
import lime.ui.KeyModifier;


import mme.core.Renderer;
import mme.core.Mesh;
import mme.core.Scene;
import mme.core.Camera;
import mme.core.RGBAColor;

import mme.material.PhongMaterial;

import mme.geometry.MeshGeometry;
import mme.geometry.Plane;

import mme.camera.PerspectiveCamera;

import mme.light.AmbientLight;
import mme.light.PointLight;

import mme.util.loader.WaveObjLoader;
import mme.util.loader.StlLoader;

using mme.math.glmatrix.Vec3Tools;


class Materials extends Application {

    private var renderer : Renderer;

    private var box_1 : MeshGeometry;
    private var box_2 : MeshGeometry;
    private var box_3 : MeshGeometry;
    private var box_4 : MeshGeometry;
    private var box_5 : MeshGeometry;
    private var box_6 : MeshGeometry;
    private var box_7 : MeshGeometry;
    private var box_8 : MeshGeometry;
    private var box_9 : MeshGeometry;

    private var camera : Camera;
    private var group : Group;

    private var scene : Scene;

    private var rotationActive : Bool;
    private var rotationSpeed : Float;
    private var rotationDirection : Float;
    private var rotationAngle : Float;

    public function new() {

        super();

        trace("Mini-Mighty-Engine v0.1 - Materials demo");
        Log.info("Mini-Mighty-Engine v0.1 - Materials demo");
    }

    public override function onRenderContextLost() : Void {
        trace("onRenderContextLost");
    }

    public override function onRenderContextRestored( context:RenderContext ) : Void {
        trace("onRenderContextRestored");
    }

    public override function onPreloadComplete() : Void {
        trace("onPreloadComplete");

        init();
    }

    private function init() : Void {

        initApp();


        switch( window.context.type ) {

            case CAIRO:
                Log.error("Render context not supported: CAIRO");
                window.close();

            case CANVAS:
                Log.error("Render context not supported: CANVAS");
                window.close();

            case DOM:
                Log.error("Render context not supported: DOM");
                window.close();

            case FLASH:
                Log.error("Render context not supported: FLASH");
                window.close();

            case OPENGL, OPENGLES, WEBGL:

            #if ( lime_opengl || lime_opengles )
                trace( "OpenGL version: " + GL.getString( GL.VERSION ) );
                trace( "GLSL version: " + GL.getString( GL.SHADING_LANGUAGE_VERSION ) );
            #end

                var gl = window.context.webgl;

                renderer = new Renderer( gl );

                initScene( gl );

            default:

                Log.error("Current render context not supported by this sample");
                window.close();

        }
    }

    private function initApp() : Void {

        rotationActive = true;
        rotationSpeed = 45.0; // in degrees per second
        rotationDirection = -1.0;
        rotationAngle = 0.0;
    }

    private function initScene( gl : WebGLRenderContext ) : Void {

		//
		// MATERIALS
		//
        // 0x00ff00ff - green
        // 0xfc9b0aff - orange
        // 0xd80afcff - purple
        var ks : Vec3 = RGBAColor.fromInt( 0x00ff00ff );
        var material_1 = new PhongMaterial( 0x00ff00ff, 16.0 );
        material_1.kd = material_1.kd.scale( 0.7 );
        material_1.ks = ks.scale( 0.4 );
        var material_2 = new PhongMaterial( 0x00ff00ff, 64.0 );
        material_2.kd = material_2.kd.scale( 0.7 );
        material_2.ks = ks.scale( 0.4 );
        var material_3 = new PhongMaterial( 0x00ff00ff, 256.0 );
        material_3.kd = material_3.kd.scale( 0.7 );
        material_3.ks = ks.scale( 0.4 );

        var material_4 = new PhongMaterial( 0x00ff00ff, 16.0 );
        material_4.kd = material_4.kd.scale( 0.3 );
        material_4.ks = ks.scale( 0.1 );
        var material_5 = new PhongMaterial( 0x00ff00ff, 64.0 );
        material_5.kd = material_5.kd.scale( 0.3 );
        material_5.ks = ks.scale( 0.1 );
        var material_6 = new PhongMaterial( 0x00ff00ff, 256.0 );
        material_6.kd = material_6.kd.scale( 0.3 );
        material_6.ks = ks.scale( 0.1 );

        var material_7 = new PhongMaterial( 0x00ff00ff, 16.0 );
        material_7.kd = material_7.kd.scale( 0.2 );
        material_7.ks = ks.scale( 0.8 );
        var material_8 = new PhongMaterial( 0x00ff00ff, 64.0 );
        material_8.kd = material_8.kd.scale( 0.2 );
        material_8.ks = ks.scale( 0.8 );
        var material_9 = new PhongMaterial( 0x00ff00ff, 256.0 );
        material_9.kd = material_9.kd.scale( 0.2 );
        material_9.ks = ks.scale( 0.8 );

		//
		// SCENE
		//

        scene = new Scene();

        var aspect = window.width / window.height;

        camera = new PerspectiveCamera( 75., aspect, 1., 1000. );
        camera.translate( [ 0., 0., 980. ] );

        scene.addCamera( camera );

        group = new Group();
        group.add( camera );
        scene.add( group );

        controlledNode = camera;


        var ambientLight = new AmbientLight( 0xffffffff, 0.1 );
        scene.addLight( ambientLight );

        var pointLight = new PointLight( 0xffffffff, 1.0 );
        pointLight.translate([ 0.0, 0.0, 700.0 ]);
        scene.addLight( pointLight );


        var objLoader = new WaveObjLoader();
        var objMesh = objLoader.loadAsMesh( "assets/models/cuboid.obj", Assets.getBytes );
        objMesh.scaleTo( 60., 60., 10. );



        box_1 = new MeshGeometry( objMesh, material_1 );
        box_1.translate([ -200., 150., 560. ]);
        box_1.rotate( 0, Vec3.Y_AXIS );
        box_1.rotate( 25, Vec3.X_AXIS );
        scene.add( box_1 );

        box_2 = new MeshGeometry( objMesh, material_2 );
        box_2.translate([ 0., 150., 560. ]);
        box_2.rotate( 0, Vec3.Y_AXIS );
        box_2.rotate( 25, Vec3.X_AXIS );
        scene.add( box_2 );

        box_3 = new MeshGeometry( objMesh, material_3 );
        box_3.translate([ 200., 150., 560. ]);
        box_3.rotate( 0, Vec3.Y_AXIS );
        box_3.rotate( 25, Vec3.X_AXIS );
        scene.add( box_3 );

        box_4 = new MeshGeometry( objMesh, material_4 );
        box_4.translate([ -200., 0., 560. ]);
        box_4.rotate( 0, Vec3.Y_AXIS );
        box_4.rotate( 0, Vec3.X_AXIS );
        scene.add( box_4 );

        box_5 = new MeshGeometry( objMesh, material_5 );
        box_5.translate([ 0., 0., 560. ]);
        box_5.rotate( 0, Vec3.Y_AXIS );
        box_5.rotate( 0, Vec3.X_AXIS );
        scene.add( box_5 );

        box_6 = new MeshGeometry( objMesh, material_6 );
        box_6.translate([ 200., 0., 560. ]);
        box_6.rotate( 0, Vec3.Y_AXIS );
        box_6.rotate( 0, Vec3.X_AXIS );
        scene.add( box_6 );

        box_7 = new MeshGeometry( objMesh, material_7 );
        box_7.translate([ -200., -150., 560. ]);
        box_7.rotate( 0, Vec3.Y_AXIS );
        box_7.rotate( -25, Vec3.X_AXIS );
        scene.add( box_7 );

        box_8 = new MeshGeometry( objMesh, material_8 );
        box_8.translate([ 0., -150., 560. ]);
        box_8.rotate( 0, Vec3.Y_AXIS );
        box_8.rotate( -25, Vec3.X_AXIS );
        scene.add( box_8 );

        box_9 = new MeshGeometry( objMesh, material_9 );
        box_9.translate([ 200., -150., 560. ]);
        box_9.rotate( 0, Vec3.Y_AXIS );
        box_9.rotate( -25, Vec3.X_AXIS );
        scene.add( box_9 );
    }

    public override function update( deltaTime : Int ) : Void {

        if( !preloader.complete ) return;

        if( rotationActive ) {

            // deltaTime is in milliseconds
            var delta = deltaTime / 1000.0;
            var angle = delta * rotationSpeed;
            rotationAngle += rotationDirection * angle;
            var rotationLimit = 50.0;

            if( rotationAngle > rotationLimit || rotationAngle < -rotationLimit ) {
                rotationAngle = rotationDirection * rotationLimit;
                rotationDirection = - rotationDirection;
            }

            angle *= rotationDirection;

            box_1.rotate( angle, Vec3.Y_AXIS );
            box_2.rotate( angle, Vec3.Y_AXIS );
            box_3.rotate( angle, Vec3.Y_AXIS );
            box_4.rotate( angle, Vec3.Y_AXIS );
            box_5.rotate( angle, Vec3.Y_AXIS );
            box_6.rotate( angle, Vec3.Y_AXIS );
            box_7.rotate( angle, Vec3.Y_AXIS );
            box_8.rotate( angle, Vec3.Y_AXIS );
            box_9.rotate( angle, Vec3.Y_AXIS );
        }
    }

    public override function onMouseDown( x : Float, y : Float, button : MouseButton ) : Void {

        rotationActive = ! rotationActive;
    }

    var controlledNode : SceneNode;

    public override function onKeyDown( keyCode : KeyCode, modifier : KeyModifier ) : Void {

        var rotation = 5.0; // in degrees
        var translation = 10.0;

        switch( keyCode ) {

            case KeyCode.ESCAPE | KeyCode.Q:
                window.close();

            case KeyCode.C:
                if( controlledNode == camera ) {
                    controlledNode = group;
                    Sys.println( 'controlled node is group' );
                } else if( controlledNode == group ) {
                    controlledNode = camera;
                    Sys.println( 'controlled node is camera' );
                }

            case KeyCode.W:
                controlledNode.translateY( - translation );
                Sys.println('viewTransform ${camera.viewTransform}');

            case KeyCode.S:
                controlledNode.translateY( translation );
                Sys.println('viewTransform ${camera.viewTransform}');

            case KeyCode.A:
                controlledNode.translateX( - translation );
                Sys.println('viewTransform ${camera.viewTransform}');
                Sys.println('camera.position ${camera.positionX}');
                Sys.println('group.position ${group.positionX}');

            case KeyCode.D:
                controlledNode.translateX( translation );
                Sys.println('viewTransform ${camera.viewTransform}');
                Sys.println('camera.position ${camera.positionX}');
                Sys.println('group.position ${group.positionX}');

            case KeyCode.UP:
                box_1.rotate( - rotation, Vec3.X_AXIS );
                box_2.rotate( - rotation, Vec3.X_AXIS );
                box_3.rotate( - rotation, Vec3.X_AXIS );
                box_4.rotate( - rotation, Vec3.X_AXIS );
                box_5.rotate( - rotation, Vec3.X_AXIS );
                box_6.rotate( - rotation, Vec3.X_AXIS );
                box_7.rotate( - rotation, Vec3.X_AXIS );
                box_8.rotate( - rotation, Vec3.X_AXIS );
                box_9.rotate( - rotation, Vec3.X_AXIS );

            case KeyCode.DOWN:
                box_1.rotate( rotation, Vec3.X_AXIS );
                box_2.rotate( rotation, Vec3.X_AXIS );
                box_3.rotate( rotation, Vec3.X_AXIS );
                box_4.rotate( rotation, Vec3.X_AXIS );
                box_5.rotate( rotation, Vec3.X_AXIS );
                box_6.rotate( rotation, Vec3.X_AXIS );
                box_7.rotate( rotation, Vec3.X_AXIS );
                box_8.rotate( rotation, Vec3.X_AXIS );
                box_9.rotate( rotation, Vec3.X_AXIS );

            case KeyCode.RIGHT:
                box_1.rotate( rotation, Vec3.Y_AXIS );
                box_2.rotate( rotation, Vec3.Y_AXIS );
                box_3.rotate( rotation, Vec3.Y_AXIS );
                box_4.rotate( rotation, Vec3.Y_AXIS );
                box_5.rotate( rotation, Vec3.Y_AXIS );
                box_6.rotate( rotation, Vec3.Y_AXIS );
                box_7.rotate( rotation, Vec3.Y_AXIS );
                box_8.rotate( rotation, Vec3.Y_AXIS );
                box_9.rotate( rotation, Vec3.Y_AXIS );

            case KeyCode.LEFT:
                box_1.rotate( - rotation, Vec3.Y_AXIS );
                box_2.rotate( - rotation, Vec3.Y_AXIS );
                box_3.rotate( - rotation, Vec3.Y_AXIS );
                box_4.rotate( - rotation, Vec3.Y_AXIS );
                box_5.rotate( - rotation, Vec3.Y_AXIS );
                box_6.rotate( - rotation, Vec3.Y_AXIS );
                box_7.rotate( - rotation, Vec3.Y_AXIS );
                box_8.rotate( - rotation, Vec3.Y_AXIS );
                box_9.rotate( - rotation, Vec3.Y_AXIS );
            
            default:
        }
        
    }

    public override function render( context : RenderContext ) : Void {

        if( !preloader.complete ) return;

        switch( context.type ) {

            case OPENGL, OPENGLES, WEBGL:

                var gl = context.webgl;

                renderer.render( gl, scene );
            default:

        }
    }
}
