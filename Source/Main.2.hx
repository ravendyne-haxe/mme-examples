package;


import mme.math.glmatrix.Vec4;
import mme.core.SceneObject;
import haxe.io.BytesInput;
import mme.util.loader.WaveObjLoader;
import mme.light.AmbientLight;
import lime.utils.Log;
import lime.utils.Assets;
import lime.graphics.ImageFileFormat;
import lime.graphics.Image;
import lime.graphics.PixelFormat;
import lime.graphics.ImageBuffer;
import lime.utils.UInt8Array;
import lime.graphics.opengl.GLFramebuffer;
import lime.graphics.opengl.GLRenderbuffer;
import lime.graphics.opengl.GLBuffer;
import lime.graphics.opengl.GLTexture;
import lime.graphics.opengl.GL;
import lime.graphics.OpenGLRenderContext;
import lime.ui.MouseWheelMode;
import mme.material.TextureMaterial;
import mme.geometry.Square;
import mme.core.Mesh;
import mme.geometry.MeshGeometry;
import mme.g2d.material.Point2DMaterial;
import mme.material.NamedColor;
import mme.core.Group;
import mme.g2d.material.Line2DMaterial;
import mme.math.glmatrix.Vec3;
import mme.math.Easing;
import lime.math.RGBA;
import mme.material.BasicMaterial;
import mme.geometry.Rectangle;
import mme.math.glmatrix.Vec2;
import lime.math.Vector4;
import mme.core.Renderer;
import mme.g2d.geometry.PolyLine2D;
import mme.geometry.PolyLine;
import mme.g2d.geometry.PointCloud2D;
import mme.geometry.Plane;
import mme.camera.OrthoCamera;
import mme.core.Scene;
import lime.ui.KeyCode;
import lime.ui.KeyModifier;
import lime.ui.MouseButton;

import lime.app.Application;
import lime.graphics.RenderContext;
import lime.graphics.WebGLRenderContext;

using mme.math.glmatrix.Vec3Tools;

import mme.extras.camera.OrthoCameraController;
import mme.material.PhongMaterial;
import mme.camera.PerspectiveCamera;
import mme.core.Camera;
import mme.core.SceneNode;
import mme.light.PointLight;

import mme.geometry.Box;

import mme.math.Parametric;
import mme.math.Parametric.PlanarGenerators;

import mme.util.Utils;

import mme.geometry.Sphere;
import mme.geometry.Cone;
import mme.geometry.Circle;
import mme.geometry.Cylinder;
import mme.extras.geometry.Paraboloid;

import mme.g2d.geometry.LineCollection2D;

import mme.core.BoundingBox;

import mme.math.delaunator.Delaunator;
import mme.math.delaunator.Bench;

using mme.math.glmatrix.Vec4Tools;
using mme.math.glmatrix.Vec3Tools;
using mme.math.glmatrix.Vec2Tools;
using mme.math.glmatrix.Mat3Tools;
using mme.math.glmatrix.Mat4Tools;

import mme.extras.camera.OrbitCameraController;

import mme.material.WireframeMaterial;


class Main extends Application {

    var scene : Scene;
    var ortho_camera : Camera;
    var persp_camera : Camera;
    var polyline : PolyLine2D;
    var plate : Plane;
    var rectangle : Rectangle;
    var light : PointLight;

    var arrow : MeshGeometry;
    var plateGorup : Group;
    var ttest : PolyLine;

    var renderer : Renderer;

    var o_ctrlr : OrbitCameraController;
    var p_ctrlr : OrbitCameraController;

    public function new() {

        super();

        trace("Mini-Mighty-Engine - Main.hx");
    }

    public override function onPreloadComplete() : Void {
        trace("onPreloadComplete");
    #if ( lime_opengl || lime_opengles )
        trace( "OpenGL version: " + GL.getString( GL.VERSION ) );
        trace( "GLSL version: " + GL.getString( GL.SHADING_LANGUAGE_VERSION ) );
    #end
        trace( window.context.type );
        trace( window.context.version );

        makeit();
    }

    function makeit() {
        scene = new Scene();
        renderer = new Renderer( window.context.webgl );
        ortho_camera = new OrthoCamera( 0, 500, 0, 500, 1000 );
        scene.addCamera( ortho_camera );
    }

    public override function onKeyDown( keyCode : KeyCode, modifier : KeyModifier ) : Void {

        var rotation = 5.0; // in degrees

        switch( keyCode ) {

            case KeyCode.ESCAPE:
                window.close();

            case KeyCode.Q:
                window.close();

            case KeyCode.T:

                var points = new PointCloud2D( 10, 10, NamedColor.orange );
                points.addPoints([
                    [ 100, 100 ],
                    [ 200, 200 ],
                    [ 300, 300 ]
                ]);
                Utils.printMesh( points.mesh );
                scene.add( points );


            default:
        }
    }

    public override function render( context : RenderContext ) : Void {

	    if (!preloader.complete) return;

	    switch (context.type) {

		    case OPENGL, OPENGLES, WEBGL:

                var gl = context.webgl;

                renderer.render( gl, scene );


		    default:

		}
	}
}
