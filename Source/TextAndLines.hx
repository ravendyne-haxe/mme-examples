package;

import mme.extras.core.OrthoScene;
import haxe.Timer;
import mme.MMEngine;
import mme.extras.camera.CameraTools;
import mme.core.SceneObject;
import mme.light.AmbientLight;
import lime.utils.Log;
import lime.graphics.opengl.GL;
import mme.material.NamedColor;
import mme.core.Group;
import mme.material.BasicMaterial;
import mme.geometry.Rectangle;
import mme.core.Renderer;
import mme.g2d.geometry.PolyLine2D;
import mme.geometry.PolyLine;
import mme.geometry.Plane;
import mme.camera.OrthoCamera;
import mme.core.Scene;
import lime.ui.KeyCode;
import lime.ui.KeyModifier;
import lime.ui.MouseButton;

import lime.app.Application;
import lime.graphics.RenderContext;

using mme.math.glmatrix.Vec3Tools;

import mme.camera.PerspectiveCamera;
import mme.core.Camera;
import mme.light.PointLight;

import mme.geometry.Box;

import mme.extras.camera.OrbitCameraController;

import mme.geometry.Plane;
import mme.core.Group;
import lime.utils.Log;
import mme.core.BoundingBox;
import lime.utils.Assets;
import mme.math.glmatrix.Vec3;
import mme.util.Utils;
import mme.util.loader.WaveObjLoader;
import mme.core.Scene;

import mme.font.FontSource;
import mme.font.FontAtlas;
import mme.g2d.geometry.TextLabel2D;
import mme.material.PhongMaterial;
import mme.material.LineMaterial;
import mme.g2d.geometry.PointCloud2D;


class TextAndLines extends Application {

    var scene : Scene;
    var ortho_camera : Camera;
    var persp_camera : Camera;
    var light : PointLight;

    var renderer : Renderer;

    var o_ctrlr : OrbitCameraController;
    var p_ctrlr : OrbitCameraController;

    public function new() {

        super();

        trace("Mini-Mighty-Engine - Main.hx");

    }

    override function onWindowCreate() {

        var gl = window.context.webgl;
        renderer = new Renderer( gl );
    }

    // override function onPreloadProgress( loaded : Int, total : Int ) {
    //     Log.debug('$loaded of $total');
    // }

    public override function onPreloadComplete() : Void {
        trace("onPreloadComplete");
    #if ( lime_opengl || lime_opengles )
        trace( "OpenGL version: " + GL.getString( GL.VERSION ) );
        trace( "GLSL version: " + GL.getString( GL.SHADING_LANGUAGE_VERSION ) );
    #end
        trace( window.context.type );
        trace( window.context.version );
        
        // Text.createFontAtlasBinary();

        var start = Timer.stamp();

        MMEngine.initialize();

        createScene();
        makeSceneContent();

        var delta = Timer.stamp() - start;
        Log.info('load -> ${delta * 1000} ms');
    }

    function createScene() {

        scene = new Scene();

        var aspect = window.width / window.height;

        ortho_camera = new OrthoCamera( 0, 1000 * aspect, 0, 1000, 10000.0 );
        // ortho_camera = new OrthoCamera( -500, 500, -500, 500, 2000.0 );
        ortho_camera.translateZ( 1000 );
        scene.addCamera( ortho_camera );
        // Log.info('ortho_camera ${ortho_camera.scenePosition.str()}');
        Log.info('ortho_camera ${ortho_camera.scenePosition}');

        o_ctrlr = new OrbitCameraController( ortho_camera, window );
        scene.add( o_ctrlr );

        // persp_camera = new PerspectiveCamera( 60, 1, 1, 10000 );
        persp_camera = new PerspectiveCamera( 60, aspect, 1, 10000 );
        // persp_camera.translateZ( 1000 );
        persp_camera.translate([ 500, 500, 1000 ]);
        scene.addCamera( persp_camera );
        // Log.info('persp_camera ${persp_camera.scenePosition.str()}');
        Log.info('persp_camera ${persp_camera.scenePosition}');

        p_ctrlr = new OrbitCameraController( persp_camera, window );
        scene.add( p_ctrlr );



        //
        // POINT LIGHTS
        //
        light = new PointLight();
        // light.translate([ 10000, 0, 10000 ]);
        // light.translate([ -100, 0, 300 ]);
        // light.translate([ 750, 750, 500 ]);
        light.translate([ 500, 750, -400 ]);
        scene.addLight( light );
        // var light_marker = new Square( 20, new BasicMaterial( NamedColor.yellow ) );
        var light_marker = new Box( 10, 10, new BasicMaterial( NamedColor.yellow ) );
        light.add( light_marker );

        scene.add( light );
        // OR
        // p_ctrlr.add( light );


        light = new PointLight();
        light.translate([ -500, 750, -400 ]);
        scene.addLight( light );
        var light_marker_2 = new Box( 10, 10, new BasicMaterial( NamedColor.yellow ) );
        light.add( light_marker_2 );

        scene.add( light );
        // OR
        // p_ctrlr.add( light );


        //
        // AMBIENT LIGHT
        //
        var ambient = new AmbientLight();
        scene.addLight( ambient );

        // light.intensity = 0;
        // ambient.intensity = 0;

        // light.intensity = 9/16;
        // ambient.intensity = 0.15;
    }

    function testLabel( scene : Scene ) {

        var testLabelHorizontal = new TextLabel2D( "AWgljmeij str", NamedColor.lime );
        testLabelHorizontal.translate([700, 600, 0]);
        testLabelHorizontal.translateZ( 50 );
        scene.add( testLabelHorizontal );

        var testLabelVertical = new TextLabel2D( "AWgljmeij str", true, NamedColor.lime );
        testLabelVertical.translate([705, 560, 0]);
        testLabelVertical.translateZ( 50 );
        scene.add( testLabelVertical );
    }

    #if !js
    function textVerticalHorizontal( scene : Scene ) {

        var fontLocation = "assets/fonts/Trivial-Regular.ttf";

        var source = new FontSource( Assets.getFont( fontLocation ) );

        var htext = FontSource.renderText( source, "Mini Mighty Engine" );
        htext.translate([ 600, 800, 100 ]);
        scene.add( htext );

        var vtext = FontSource.renderText( source, "Mini Mighty Engine", true );
        vtext.translate([ 600, 750, 100 ]);
        scene.add( vtext );
    }
    #end

    #if !js
    public static function createFontAtlasBinary() {
        //
        // loads TTF asset, convert to binary atlas data and save to file.
        //
        // Call this function once for each font you want to create atlas binary for.
        // This should be done in a small utility program that compiles as Neko or C++ target
        // so it can load TTF assets and save them to file on disk.
        //
        // Once you generated all font atlasses you need, put them into your app's assets folder
        // and now you can load them in HTML5 (or any other) target like so:
        //
        //      ... do this once ...
        //      var fontAtlasBinary = Assets.getBytes('trivial-regular.fontatlas');
        //      myFontAtlas = FontAtlas.fromBytes( fontAtlasBinary );
        //
        //      ... and then, whenever you need to render text, do ...
        //      var txt = fontAtlas.renderText( "the text", false /*is vertical*/ );
        //      txt.translate(...) etc.
        //      scene.add( txt );
        // 

        var font = Assets.getFont("assets/fonts/Trivial-Regular.ttf");
        var ll = Log.level;
        Log.level = DEBUG;
        // var fontAtlas = FontAtlas.forFont( font, "all-the-characters-you-will-need-in-your-app" );
        var fontAtlas = FontAtlas.forFont( font, " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,/;?:\"!@#$%^&*()-=_+" );
        var fontAtlasBytes = fontAtlas.toBytes();
        sys.io.File.write("trivial-regular.fontatlas").writeFullBytes( fontAtlasBytes, 0, fontAtlasBytes.length );
        Log.debug('"trivial-regular.fontatlas" created');
        Log.level = ll;
    }
    #end

    function addArrow( scene : Scene ) {

        var loader = new WaveObjLoader();
        var obj = loader.load(
            "arrow.obj",
            function(name) return Assets.getBytes('assets/models/$name'),
            new PhongMaterial( NamedColor.limegreen )
        );

        var arrow : SceneObject = cast obj.children[0];
        arrow.mesh.scaleTo( 5, 5, 5 );
        arrow.material.emissiveColor = NamedColor.green;
        scene.add(arrow);

        return arrow;
    }

    function testMiterLines( scene : Scene ) {
        var material = new LineMaterial( 4 );

        var polyline = new PolyLine( material );
        polyline.addPoint([ 0, 0, 0 ]);
        polyline.addPoint([ 400, 0, 0 ]);
        polyline.addPoint([ 0, 10, 0 ]);
        polyline.translate([ 500, 500, 500 ]);
        scene.add( polyline );

        var pointcloud = new PointCloud2D( 10.0, 10.0, 0xead446ff );
		// pointcloud.addPoints([ [ 0, 0 ], [ 400., 0 ], [ 0., 10. ] ]);
        pointcloud.addPoint([ 0, 0 ]);
        pointcloud.addPoint([ 400, 0 ]);
        pointcloud.addPoint([ 0, 10 ]);
        pointcloud.translate([ 500, 500, 500 ]);
		scene.add(pointcloud);
    }

    function lines( scene : Scene ) {

        var ttest_control = new PolyLine2D( NamedColor.fuchsia, 10 );
        ttest_control.addPoint([ 200, 800, 0 ]);
        ttest_control.addPoint([ 800, 800, 0 ]);
        ttest_control.addPoint([ 800, 400, 0 ]);
        ttest_control.translateZ( -20 );
        scene.add( ttest_control );

        var ttest = new PolyLine( NamedColor.orange, 10, NamedColor.red );
        ttest.addPoint([ 200, 800, 0 ]);
        ttest.addPoint([ 800, 800, 0 ]);
        ttest.addPoint([ 800, 400, 0 ]);
        // Utils.printMesh( ttest.mesh );
        scene.add( ttest );

        return ttest;
    }

    function makeSceneContent() {
        var start = Timer.stamp();

        //
        // REFERENCE OBJECTS
        //
        addArrow( scene );


        //
        // LINES
        //
        testMiterLines(scene);
        lines( scene );

        //
        // TEXT
        //
        testLabel( scene );
        #if !js
        textVerticalHorizontal( scene );
        #end

        var delta = Timer.stamp() - start;
        Log.info('makeSceneContent -> ${delta * 1000} ms');
    }

    public override function onWindowResize( width : Int, height : Int ) {
        // renderer.viewport( window.context.webgl, width, height );
    }

    public override function onMouseDown(x:Float, y:Float, button:MouseButton) {
        Log.info(' mouse down: $x, $y ');
        var ray = CameraTools.ray( ortho_camera, Std.int(x), Std.int(y), window.width, window.height );
        Log.info('ortho ray: ${ray.origin}, ${ray.direction} ');
        ray = CameraTools.ray( persp_camera, Std.int(x), Std.int(y), window.width, window.height );
        Log.info('persp ray: ${ray.origin}, ${ray.direction} ');
    }

    public override function onKeyDown( keyCode : KeyCode, modifier : KeyModifier ) : Void {

        var rotation = 5.0; // in degrees

        switch( keyCode ) {

            case ESCAPE, Q:
                window.close();


            case W:
                // plate.rotate( -10, Vec3.X_AXIS );
                var whereTo = scene.activeCamera.cameraLook.scale(20);
                p_ctrlr.translate(whereTo);
                o_ctrlr.translate(whereTo);

            case S:
                // plate.rotate( 10, Vec3.X_AXIS );
                var whereTo = scene.activeCamera.cameraLook.scale(-20);
                p_ctrlr.translate(whereTo);
                o_ctrlr.translate(whereTo);

            case A:
                // plate.rotate( -10, Vec3.Y_AXIS );
                var whereTo = scene.activeCamera.cameraLook.cross( scene.activeCamera.cameraUp ).scale(-20);
                p_ctrlr.translate(whereTo);
                o_ctrlr.translate(whereTo);

            case D:
                // plate.rotate( 10, Vec3.Y_AXIS );
                var whereTo = scene.activeCamera.cameraLook.cross( scene.activeCamera.cameraUp ).scale(20);
                p_ctrlr.translate(whereTo);
                o_ctrlr.translate(whereTo);

            case C:
                if( scene.activeCamera == persp_camera ) {
                    scene.activeCamera = ortho_camera;
                } else {
                    scene.activeCamera = persp_camera;
                }

            case R:
                p_ctrlr.reset();
                p_ctrlr.position = [0,0,0];
                o_ctrlr.reset();
                o_ctrlr.position = [0,0,0];

            case UP:
                p_ctrlr.orbitTilt( rotation );
                o_ctrlr.orbitTilt( rotation );

            case DOWN:
                p_ctrlr.orbitTilt( - rotation );
                o_ctrlr.orbitTilt( - rotation );

            case RIGHT:
                if( modifier.ctrlKey ) {
                    p_ctrlr.orbitPan( - 45 );
                    o_ctrlr.orbitPan( - 45 );
                }
                else{
                    p_ctrlr.orbitPan( - rotation );
                    o_ctrlr.orbitPan( - rotation );
                }

            case LEFT:
                if( modifier.ctrlKey ){
                    p_ctrlr.orbitPan( 45 );
                    o_ctrlr.orbitPan( 45 );
                }
                else{
                    p_ctrlr.orbitPan( rotation );
                    o_ctrlr.orbitPan( rotation );
                }

            default:
        }
    }

    public override function render( context : RenderContext ) : Void {

	    if (!preloader.complete) {
            return;
        }

	    switch (context.type) {

		    case OPENGL, OPENGLES, WEBGL:

                var gl = context.webgl;

                if( renderer != null )
                renderer.render( gl, scene );
                else {
                    gl.clearColor( 0, 1, 0, 1 );
                    gl.clear( GL.COLOR_BUFFER_BIT );
                }


		    default:

		}
	}
}
