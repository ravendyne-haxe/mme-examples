package;

import mme.core.SceneObject;
import lime.math.RGBA;
import mme.core.SceneNode;
import mme.material.NamedColor;
import mme.math.glmatrix.Vec3;
import lime.utils.Assets;
import lime.app.Application;
import lime.utils.Log;

import lime.graphics.RenderContext;
import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

import lime.ui.MouseButton;
import lime.ui.KeyCode;
import lime.ui.KeyModifier;


import mme.core.Renderer;
import mme.core.Mesh;
import mme.core.Scene;
import mme.core.Camera;
import mme.core.Group;

import mme.material.TextureMaterial;
import mme.material.BasicMaterial;
import mme.material.PhongMaterial;

import mme.geometry.Triangle;
import mme.geometry.Rectangle;
import mme.geometry.Square;
import mme.geometry.MeshGeometry;

import mme.camera.PerspectiveCamera;

import mme.light.AmbientLight;
import mme.light.PointLight;

import mme.util.loader.WaveObjLoader;
import mme.util.loader.StlLoader;


class GroupsAndCameras extends Application {

    private var renderer : Renderer;

    private var scene : Scene;
    private var camera_1 : Camera;
    private var camera_2 : Camera;

    private var objMeshGeometry : MeshGeometry;
    private var stlMeshGeometry : MeshGeometry;

    private var groupNode : Group;

    private var timePassed : Int;

    private var doRotate : Bool;

    public function new() {

        super();

        trace("Mini-Mighty-Engine v0.1 - Groups and cameras demo");
        Log.info("Mini-Mighty-Engine v0.1 - Groups and cameras demo");
    }

    public override function onRenderContextLost() : Void {
        trace("onRenderContextLost");
    }

    public override function onRenderContextRestored( context:RenderContext ) : Void {
        trace("onRenderContextRestored");
    }

    public override function onPreloadComplete() : Void {
        trace("onPreloadComplete");

        init();
    }

    private function init() : Void {

        initApp();


        switch( window.context.type ) {

            case CAIRO:
                Log.error("Render context not supported: CAIRO");
                window.close();

            case CANVAS:
                Log.error("Render context not supported: CANVAS");
                window.close();

            case DOM:
                Log.error("Render context not supported: DOM");
                window.close();

            case FLASH:
                Log.error("Render context not supported: FLASH");
                window.close();

            case OPENGL, OPENGLES, WEBGL:

            #if ( lime_opengl || lime_opengles )
                trace( "OpenGL version: " + GL.getString( GL.VERSION ) );
                trace( "GLSL version: " + GL.getString( GL.SHADING_LANGUAGE_VERSION ) );
            #end

                var gl = window.context.webgl;

                renderer = new Renderer( gl );

                initScene( gl );

            default:

                Log.error("Current render context not supported by this sample");
                window.close();

        }
    }

    private function initApp() : Void {

        timePassed = 0;
        doRotate = true;
    }

    function cameraGizmo( color : RGBA ) : SceneObject {

        // var gizmo = new Square( 10, new BasicMaterial( color ) );
        var loader = new WaveObjLoader();
        var mesh = loader.loadAsMesh( "assets/models/arrow.obj", Assets.getBytes );
        mesh.scaleTo( 2, 2, 2 );
        mesh.moveTo([ 5, 0, 0 ]);
        var gizmo = new MeshGeometry( mesh, new BasicMaterial( color ) );

        return gizmo;
    }

    private function initScene( gl : WebGLRenderContext ) : Void {


        var orange = new BasicMaterial( NamedColor.orange );
        var green = new BasicMaterial( NamedColor.green );
        var purple = new BasicMaterial( NamedColor.purple );

        var distanceA = 550.;
        var distanceB = 200.;

        var phongGreen = new PhongMaterial( NamedColor.lime );
        var phongOrange = new PhongMaterial( NamedColor.orange );



		//
		// SCENE
		//

        scene = new Scene();

		//
		// FIRST CAMERA
		//

        var aspect = window.width / window.height;

        camera_1 = new PerspectiveCamera( 75., aspect, 1., 2000. );
        camera_1.translate( [ 0., 0., 880. ] );
        scene.addCamera( camera_1 );


		//
		// LIGHTS
		//

        var ambientLight = new AmbientLight( 0xffffffff, 0.1 );
        scene.addLight( ambientLight );

        var pointLight = new PointLight( 0xffffffff, 1.0 );
        pointLight.translate( [ 100.0, 0.0, 700.0 ] );
        scene.addLight( pointLight );

		//
		// TWO MESHES
		//

		// OBJ model
		//
        var objLoader = new WaveObjLoader();
        var objMesh = objLoader.loadAsMesh( "assets/models/icosahedron.obj", Assets.getBytes );
        objMesh.scaleTo( 50., 50., 50. );

        objMeshGeometry = new MeshGeometry( objMesh, phongOrange );
        objMeshGeometry.translate( [ -75., 50., 560. ] );
        objMeshGeometry.rotate( 45, Vec3.Y_AXIS );
        objMeshGeometry.rotate( 30, Vec3.X_AXIS );
        scene.add( objMeshGeometry );

		// STL model
		//
        var stlLoader = new StlLoader();
        var stlMesh = stlLoader.load( 'assets/models/hole.stl' );
        stlMesh.scaleTo( 2., 2., 2. );

        stlMeshGeometry = new MeshGeometry( stlMesh, phongGreen );
        stlMeshGeometry.translate( [ 0., -100., 570. ] );
        scene.add( stlMeshGeometry );


		//
		// SECOND CAMERA
		//

        // Groups are non-renderable scene nodes (like cameras)
        // We'll add() camera #2 to this Group node, and then translate it
        // on Z-axis, positioning it a bit away from the Group node's center.
        // Calling rotate() method on the Group node will rotate the camera
        // around the Group node's center.
        groupNode = new Group();
        // position the group node about halfway between two meshes loaded before
        groupNode.translate( [ 0.0, 0.0, 560. ] );
        scene.add( groupNode );

        camera_2 = new PerspectiveCamera( 75., aspect, 1., 2000. );
        // adding a camera to the scene will make it active
        scene.addCamera( camera_2 );
        // adding a camera to a node in a scene will have an
        // effect that all transformations on the node (translations, rotations)
        // and it's parent nodes, will also be applied to the camera.
        // NOTE: adding a Camera instance to a scene node will NOT
        // addCamera() to the scene. That method has to be called separately,
        // or you can assign camera to 'activeCamera' property which
        // will automatically add it to the scene's list of cameras.
        groupNode.add( camera_2 );
        // now that 'camera_2' is attached to 'groupNode' any transform
        // applied to 'groupNode', i.e. rotation, will be added to whatever
        // transform we do on 'camera_2'.
        // This will effectively translate 'camera_2' relative to 'groupNode'
        camera_2.translate( [ 0., 0., 200. ] );


        // In order for us to see where a Group node is,
        // simple trick is to just add an instance of SceneObject to it.
        // Here we use green square for 'groupNode', orange for 'camera_2'
        // and purple for 'camera_1'
        groupNode.add( new Square( 20., green ) );
        camera_2.add( new Square( 20., orange ) );
        // In order for anything added as a child of Camera node
        // to be rendered, we have to add() Camera to scene node tree.
        // addCamera() does NOT add a Camera to node tree so we can calculate
        // camera's view transform faster.
        // camera_1.add( new Square( 20., purple ) );
        camera_1.add( cameraGizmo( NamedColor.purple ) );
        scene.add( camera_1 );

        // make camera_2 active one at the start
        scene.activeCamera = camera_2;

        controlledObject = camera_1;
    }

    private function switchCameras() {

        if( scene.activeCamera == camera_1 ) {

            scene.activeCamera = camera_2;
            Sys.println('activeCamera camera_2');

        } else {

            scene.activeCamera = camera_1;
            Sys.println('activeCamera camera_1');
        }
    }

    public override function onMouseDown( x : Float, y : Float, button : MouseButton ) : Void {

        switchCameras();
    }

    public override function update( deltaTime : Int ) : Void {

        if( !preloader.complete ) return;

        if( ! doRotate ) return;

        timePassed += deltaTime;

        // switch cameras every 7000 milliseconds
        if( timePassed > 7000 ) {

            timePassed = 0;

            switchCameras();
        }

        // deltaTime is in milliseconds
        var rotationSpeed = 90.0; // in degrees per second
        var delta = deltaTime / 1000.0;

        objMeshGeometry.rotate( delta * rotationSpeed, Vec3.Y_AXIS );
        stlMeshGeometry.rotate( delta * rotationSpeed, Vec3.X_AXIS );

        // This rotates 'groupNode' around it's Y-axis and, effectively,
        // 'camera_2' around 'groupNode'
        groupNode.rotate( delta * rotationSpeed, Vec3.Y_AXIS );
    }

    var controlledObject : SceneNode;
    private function switchControlledCamera() {

        if( controlledObject == camera_1 ) {

            controlledObject = camera_2;
            Sys.println('controlling camera_2');

        } else if( controlledObject == camera_2 ) {

            controlledObject = groupNode;
            Sys.println('controlling groupNode');

        } else {

            controlledObject = camera_1;
            Sys.println('controlling camera_1');
        }
    }

    public override function onKeyDown( keyCode : KeyCode, modifier : KeyModifier ) : Void {

        var rotation = 5.0; // in degrees

        switch( keyCode ) {

            case KeyCode.ESCAPE | KeyCode.Q:
                window.close();

            case KeyCode.P:
                doRotate = ! doRotate;
                Sys.println('doRotate = ${doRotate}');

            case KeyCode.C:
                switchControlledCamera();

            case KeyCode.UP:
                controlledObject.rotate( rotation, Vec3.X_AXIS );

            case KeyCode.DOWN:
                controlledObject.rotate( - rotation, Vec3.X_AXIS );

            case KeyCode.RIGHT:
                controlledObject.rotate( - rotation, Vec3.Y_AXIS );

            case KeyCode.LEFT:
                controlledObject.rotate( rotation, Vec3.Y_AXIS );
            
            default:
        }
        
    }

    public override function render( context : RenderContext ) : Void {

        if( !preloader.complete ) return;

        switch( context.type ) {

            case OPENGL, OPENGLES, WEBGL:

                var gl = context.webgl;

                renderer.render( gl, scene );
            default:

        }
    }
}
    // icosahedron : @[ -75., 50., 560. ]%scene
    // hole : @[ 0., -100., 570. ]%scene
    // groupNode : green, rotating @[ 0.0, 0.0, 560. ]%scene
    // camera_1 : purple, static @[ 0., 0., 880. ]%scene
    // camera_2 : orange, rotating with groupNode @[ 0., 0., 200. ]%groupNode
