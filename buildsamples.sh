
#Declare array with 4 elements
DEMOS=(
    "demo-cube"
    "demo-device"
    "demo-loaders"
    "demo-two-scenes"
    "demo-groups-and-cameras"
    "demo-materials"
    "demo-materials-2"
    "demo-lines-2d"
    "demo-points-2d"
    "demo-ortho-controller"
    "demo-terrain"
    "demo-lowpolytrees"
    "demo-text-and-lines"
)

# get number of elements in the array
ELEMENTS=${#DEMOS[@]}
TARGET="$1"

if [ -z "$TARGET" ]; then
    TARGET=neko
fi

echo "Building for target '$TARGET'..."

# echo each element in array 
# for loop
for (( i=0;i<$ELEMENTS;i++)); do
    name=${DEMOS[${i}]}
    echo "lime build $TARGET -D$name"
    lime build neko -D$name
    if [ $? -ne 0 ]; then
        exit $?
    fi
done 

echo "All done."
